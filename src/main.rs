#![no_main]
#![no_std]

extern crate alloc;
extern crate boot_loader_spec;

use alloc::vec::Vec;

use core::fmt::Write;

use uefi::data_types::CStr16;
use uefi::prelude::*;
use uefi::proto::media::file::{Directory, File, FileAttribute, FileMode, FileType};

fn crawl_tree(system_table: &mut SystemTable<Boot>, dir: &mut Directory, depth: u8) {
    let mut buf: Vec<u8> = Vec::new();
    loop {
        match dir.read_entry(&mut buf[..]).map_err(|err| err.split()) {
            Ok(Some(f)) => {
                let filename = f.file_name();
                // Compare with "." and ".."
                if filename == unsafe { CStr16::from_u16_with_nul_unchecked(&[0x002E, 0x0]) }
                    || filename
                        == unsafe { CStr16::from_u16_with_nul_unchecked(&[0x002E, 0x002E, 0x0]) }
                {
                    continue;
                }

                for _ in 0..depth {
                    let _ = system_table.stdout().write_str("    ");
                }
                let _ = system_table.stdout().output_string(filename);

                if let Ok(f) =
                    dir.handle()
                        .open(f.file_name(), FileMode::Read, FileAttribute::READ_ONLY)
                {
                    match f.into_type() {
                        Ok(FileType::Dir(mut d)) => {
                            let _ = system_table.stdout().write_str("/\n");
                            crawl_tree(system_table, &mut d, depth + 1);
                        }
                        Ok(FileType::Regular(_)) => {
                            let _ = system_table.stdout().write_str("\n");
                        }
                        _ => {}
                    }
                }
            }
            Ok(None) => {
                break;
            }
            Err((_, Some(new_size))) => {
                buf.extend((0..new_size - buf.len()).map(|_| 0));
            }
            Err((status, None)) => panic!("Can't read root dir status: {:?}", status),
        };
    }
}

#[entry]
fn main(handle: Handle, mut system_table: SystemTable<Boot>) -> Status {
    uefi_services::init(&mut system_table).unwrap();

    // We get the SimpleFileSystem interface for the filesystem the LoadedImage was opened from
    let mut root = {
        let sfs_handle = system_table
            .boot_services()
            .get_image_file_system(handle)
            .expect("Failed to retrieve boot file system");

        unsafe { &mut *sfs_handle.interface.get() }
            .open_volume()
            .expect("Could not open root volume")
    };

    let _ = system_table
        .stdout()
        .write_str("Ficheros en la particion EFI: \n");

    crawl_tree(&mut system_table, &mut root, 0);

    loop {}
}
